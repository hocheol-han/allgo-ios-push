//
//  PreviewView.swift
//  allgoPush
//
//  Created by 한호철 on 2022/11/10.
//

import UIKit
import AVFoundation
import Photos

class PreviewView: UIView {
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
    
    /// Convenience wrapper to get layer as its statically known type.
    var videoPreviewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
}
