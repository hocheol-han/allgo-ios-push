//
//  ViewController.swift
//  allgoPush
//
//  Created by 한호철 on 2022/10/24.
//

import UIKit
import Photos
import MLImage
import MLKit

class ViewController: UIViewController {

//    @IBOutlet var personIV: UIImageView!
    var permissionNoArray : Array<String> = []
//    var previewView: PreviewView!
    @IBOutlet var cameraview: UIView!
    @IBOutlet var captureview: UIImageView!
    
    var captureSession: AVCaptureSession!
    var photoOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setInit()
    }
    
    func setInit(){
        
//        personIV.image = UIImage(named: "kim_da_mi.jpg")
       
        // [권한 설정 퍼미션 확인 실시]
        checkCameraPermission() // 카메라 사용 권한
        checkAlbumPermission() // 앨범 접근 권한
        
        // [권한 설정 퍼미션 확인 실시]
        if permissionNoArray.count > 0 && permissionNoArray.isEmpty == false {
            self.intentAppSettings(content: permissionNoArray.description+" 권한이 비활성화 상태입니다. 권한 설정으로 이동하시겠습니까?")
        }
        else {
//            self.showAlert(tittle: "권한 설정 확인", content: "모든 권한이 정상 부여되었습니다", okBtb: "확인", noBtn: "")
            print("bbbbbbbbb")
        }
//        faceDetect()
        
    }
    
    /*
        [카메라 권한 요청]
        필요 : import AVFoundation
        */
        func checkCameraPermission(){
            print("")
            print("===============================")
            print("[MainController > checkCameraPermission() : 카메라 권한 요청 실시]")
            print("===============================")
            print("")
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    print("")
                    print("===============================")
                    print("[MainController > checkCameraPermission() : 카메라 권한 허용 상태]")
                    print("===============================")
                    print("")
                } else {
                    print("")
                    print("===============================")
                    print("[MainController > checkCameraPermission() : 카메라 권한 거부 상태]")
                    print("===============================")
                    print("")
                    self.permissionNoArray.append("카메라")
                }
            })
        }
        
        
        /*
        [앨범 접근 권한 요청]
        필요 : import Photos
        */
        func checkAlbumPermission(){
            print("")
            print("===============================")
            print("[MainController > checkAlbumPermission() : 앨범 접근 권한 요청 실시]")
            print("===============================")
            print("")
            PHPhotoLibrary.requestAuthorization( { status in
                switch status{
                case .authorized:
                    print("")
                    print("===============================")
                    print("[MainController > checkAlbumPermission() : 앨범 접근 권한 허용 상태]")
                    print("===============================")
                    print("")
                case .denied:
                    print("")
                    print("===============================")
                    print("[MainController > checkAlbumPermission() : 앨범 접근 권한 거부 상태]")
                    print("===============================")
                    print("")
                    self.permissionNoArray.append("앨범 및 사진 접근")
                case .restricted, .notDetermined:
                    print("")
                    print("===============================")
                    print("[MainController > checkAlbumPermission() : 앨범 접근 권한 선택 대기 상태]")
                    print("===============================")
                    print("")
                default:
                    break
                }
            })
        }


        // [애플리케이션 설정창 이동 실시 메소드]
        func intentAppSettings(content:String){
            // 앱 설정창 이동 실시
            let settingsAlert = UIAlertController(title: "권한 설정 알림", message: content, preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "확인", style: .default) { (action) in
                // [확인 버튼 클릭 이벤트 내용 정의 실시]
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    print("")
                    print("===============================")
                    print("[MainController > intentAppSettings() : 앱 설정 화면 이동]")
                    print("===============================")
                    print("")
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
            settingsAlert.addAction(okAction) // 버튼 클릭 이벤트 객체 연결
            
            let noAction = UIAlertAction(title: "취소", style: .default) { (action) in
                // [취소 버튼 클릭 이벤트 내용 정의 실시]
                return
            }
            settingsAlert.addAction(noAction) // 버튼 클릭 이벤트 객체 연결
            
            // [alert 팝업창 활성 실시]
            present(settingsAlert, animated: false, completion: nil)
            
        }

    
//    func faceDetect(){
//
//        print("bbbbbbbbbb")
//
//        lazy var vision = Vision.vision()
//
//        let faceDetector = vision.faceDetector(options: options)
//
//        let image = VisionImage(image: personIV.image)
//
//        faceDetector.process(image) { faces, error in
//          guard error == nil, let faces = faces, !faces.isEmpty else {
//            // ...
//            return
//          }
//
//            print(faces)
//          // Faces detected
//          // ...
//        }
//    }

    @IBAction func takePicture(_ sender: UIButton) {
        print("takePicture called~!")
//        print("")
//                print("===============================")
//                print("[A_Main >> openCamera() :: 카메라 열기 수행 실시]")
//                print("===============================")
//                print("")
//
//                // [SEARCH FAST] : [카메라 호출 수행]
//                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
//                    if granted {
//                        print("")
//                        print("===============================")
//                        print("[A_Main > openCamera() : 카메라 권한 허용 상태]")
//                        print("===============================")
//                        print("")
//
//                        // [카메라 열기 수행 실시]
//                        DispatchQueue.main.async {
//                            // -----------------------------------------
//                            // [사진 찍기 카메라 호출]
//                            let camera = UIImagePickerController()
//                            camera.sourceType = .camera
//                            self.present(camera, animated: false, completion: nil)
//                            // -----------------------------------------
//                        }
//                    } else {
//                        print("")
//                        print("===============================")
//                        print("[A_Main > openCamera() : 카메라 권한 거부 상태]")
//                        print("===============================")
//                        print("")
//                    }
//                })
        
        captureSession = AVCaptureSession()
        captureSession.beginConfiguration()

        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        //후면 카메라
        guard let backCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else { return }

        // 전면 카메라
        guard let frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else { return }

        do {
            let cameraInput = try AVCaptureDeviceInput(device: captureDevice)
            let backCameraInput = try AVCaptureDeviceInput(device: backCamera)
            let frontCameraInput = try AVCaptureDeviceInput(device: frontCamera)
            
            photoOutput = AVCapturePhotoOutput()
            
//            captureSession.addInput(cameraInput)
            captureSession.addInput(frontCameraInput)
            captureSession.sessionPreset = .photo
            captureSession.addOutput(photoOutput)
            captureSession.commitConfiguration()
        } catch {
            print(error)
        }

        //preview
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        DispatchQueue.main.async {
            self.videoPreviewLayer.frame = self.cameraview.bounds
        }
        videoPreviewLayer?.videoGravity = .resizeAspectFill
        self.cameraview.layer.addSublayer(videoPreviewLayer)

        captureSession.startRunning()
        
        
    }
    
    @IBAction func capture(_ sender: UIButton) {
        
        print("capture called~!!")
        photoOutput?.capturePhoto(with: AVCapturePhotoSettings(), delegate: self as AVCapturePhotoCaptureDelegate)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation() else { return }
        guard let image = UIImage(data: imageData) else { return }
        captureSession.stopRunning()
        cameraview.isHidden = true
        captureview.image = image
        
        guard let img = UIImage(named: "kim_da_mi.jpg") else {return}//captureview.image else{ return }
        // 이미지뷰에 이미지 설정
        
        // High-accuracy landmark detection and face classification
        let options = FaceDetectorOptions()
        options.performanceMode = .accurate
//        options.landmarkMode = .all
        options.contourMode = .all
        options.classificationMode = .all

        // Real-time contour detection of multiple faces
        
        let faceDetector = FaceDetector.faceDetector(options: options)
            // [END init_face]
            // Initialize a `VisionImage` object with the given `UIImage`.
        let visionImage = VisionImage(image: img)
//        img.orientation = imageOrientation(
//          deviceOrientation: UIDevice.current.orientation,
//          cameraPosition: cameraPosition)
        guard let ori = UIImage.Orientation(rawValue: 0) else { return }
        visionImage.orientation = ori//img.imageOrientation
        print("\(img.imageOrientation)")
//        guard let img = captureview.image else {
//print("111111")
//            return
//        }
        weak var weakSelf = self
        
        faceDetector.process(visionImage) { faces, error in
          guard error == nil, let faces = faces, !faces.isEmpty else {
              
              print("error : \(error)")
              if let vfaces = faces{
                  
                  print("\(vfaces)")
                  print("\(faces?.isEmpty)")
                  
              }

            return
          }
            for face in faces {
                print("bbbb")
              let frame = face.frame
              if face.hasHeadEulerAngleY {
                let rotY = face.headEulerAngleY  // Head is rotated to the right rotY degrees
              }
              if face.hasHeadEulerAngleZ {
                let rotZ = face.headEulerAngleZ  // Head is rotated upward rotZ degrees
              }

              // If landmark detection was enabled (mouth, ears, eyes, cheeks, and
              // nose available):
              if let leftEye = face.landmark(ofType: .leftEye) {
                  
                let leftEyePosition = leftEye.position
                  print("leftEyePosition : \(leftEyePosition)")
              }

              // If contour detection was enabled:
              if let leftEyeContour = face.contour(ofType: .leftEye) {
                let leftEyePoints = leftEyeContour.points
                  print("leftEyePoints : \(leftEyePoints)")
                  for leftEyePoint in leftEyePoints{
                      print("leftEyePoint : \(leftEyePoint.x)")
                      print("leftEyePoint : \(leftEyePoint.y)")
                  }
              }
              if let upperLipBottomContour = face.contour(ofType: .upperLipBottom) {
                let upperLipBottomPoints = upperLipBottomContour.points
              }

              // If classification was enabled:
              if face.hasSmilingProbability {
                let smileProb = face.smilingProbability
              }
              if face.hasRightEyeOpenProbability {
                let rightEyeOpenProb = face.rightEyeOpenProbability
              }

              // If face tracking was enabled:
              if face.hasTrackingID {
                let trackingId = face.trackingID
              }
            }
          // Faces detected
          // ...
        }
    }
    
    func imageOrientation(
      deviceOrientation: UIDeviceOrientation,
      cameraPosition: AVCaptureDevice.Position
    ) -> UIImage.Orientation {
      switch deviceOrientation {
      case .portrait:
        return cameraPosition == .front ? .leftMirrored : .right
      case .landscapeLeft:
        return cameraPosition == .front ? .downMirrored : .up
      case .portraitUpsideDown:
        return cameraPosition == .front ? .rightMirrored : .left
      case .landscapeRight:
        return cameraPosition == .front ? .upMirrored : .down
      case .faceDown, .faceUp, .unknown:
        return .up
      }
    }
    
    func convertCIImageToCGImage(inputImage: CIImage) -> CGImage!{
        let context = CIContext(options: nil)
        if context != nil{
            return context.createCGImage(inputImage, from: inputImage.extent)
        }
        return nil
    }

}

extension ViewController: AVCapturePhotoCaptureDelegate {
//    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
//        guard let videoPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer), let _ = CMSampleBufferGetFormatDescription(sampleBuffer) else {return}
//
//        let comicEffect = CIFilter(name: "CIComicEffect")
//        let cameraImage = CIImage(cvImageBuffer: videoPixelBuffer)
//
//        comicEffect!.setValue(cameraImage, forKey: kCIInputImageKey)
//
//        let cgImage = self.context.createCGImage(comicEffect!.outputImage!, from: cameraImage.extent)!
//
//        DispatchQueue.main.async {
//            let filteredImage = UIImage(cgImage: cgImage)
//            self.imageView.image = filteredImage
//        }
//    }
}



